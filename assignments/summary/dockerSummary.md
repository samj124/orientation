## Docker
![Docker Logo](https://www.docker.com/sites/default/files/d8/2019-07/Moby-logo.png)

Docker helps you manage dependencies when building large projects and deploying them in differetn environments.

### Terminologies
|  Terms       | Description     |
| :------------- | :----------: |
|  Docker | A program to develop and run applications with containers.|
| Docker Image   | It contains what is needed to run an application as a container. This includes code, runtime, libraries, environment variables and configuration files |
| Container   | It is a running instance of docker image. Multiple containers can be created from image.|
| Docker Hub   | It is like GitHub but for docker images and containers.|

### Docker Installation
1.Uninstall older versions: 

    sudo apt-get remove docker docker-engine docker.io containerd runc

2.Install CE (Community Docker Engine):   
     
      $ sudo apt-get update
      $ sudo apt-get install \
          apt-transport-https \
          ca-certificates \
          curl \
          gnupg-agent \
          software-properties-common
      $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
      $ sudo apt-key fingerprint 0EBFCD88
      $ sudo add-apt-repository \
         "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
          stable nightly test"
      $ sudo apt-get update
      $ sudo apt-get install docker-ce docker-ce-cli containerd.io

       // Check if docker is successfully installed in your system
      $ sudo docker run hello-world


### Basic Commands
| Command       | Description     |
| :------------- | :----------: |
|  docker ps | View all the containers that are running on the Docker Host.|
| docker start | Starts any stopped container(s). |
| docker stop  | Stops any running container(s).|
| docker run   | Creates containers from docker images.|
| docker rm   | Deletes the containers.|

### Docker Workflow
![Architecture](https://docs.docker.com/engine/images/architecture.svg)


