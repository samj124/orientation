## Git
![Git Logo](https://git-scm.com/images/logos/2color-lightbg@2x.png)

### What is git?
Git is a version control system that tracks every revision during software development.

### Vocabulary
| Terms       | Description     |
| :------------- | :----------: |
|  Repository | A collection of files and folders used by git to track.|
| GitLab   | Popular remote stroage solution for git repos. |
| Commit   | Saves your work. Only exists on the local repo until it is pushed to the online repo.|
| Push   | Syncs your commits to Gitlab.|
| Branch   | Git repo is similar to a tree. The trunk is the **master branch**. The branches are code instances that are different from the master branch.|
| Merge   | Integrates a bug-free branch with the master branch.|
| Clone   | Creates a copy of the online repo on the local machine.|
| Fork   | Creates an online copy of the online repo under your own name.|

### How to install git?
* _Linux_:   sudo apt-get install git
* _Windows_:   [Download the installer](https://git-scm.com/download/win) and run it.

### Git Internals
Files can reside in three main states:
* **Modified**: File is changed but not committed to the repo.
* **Staged**: Marked a modified file in its current version to go into the next snapshot.
* **Committed**: Data is saved in the local repo in the form of snapshots.

### Basic Commands
![Basic Commands](https://qph.fs.quoracdn.net/main-qimg-d151c0543baa145e6252c1ec95199963.webp) 

### Git Workflow
![Workflow Steps](https://docs.onosproject.org/images/contributing_workflow.png)
